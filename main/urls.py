from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^gifts/(?P<owner_id>[0-9]+)/$', views.list_gifts, name='list_gifts'),
    url(r'^gift/new/$', views.new_gift, name='new_gift'),
    url(r'^gift/(?P<gift_id>[0-9]+)/$', views.view_gift, name='view_gift'),
    url(r'^gift/(?P<gift_id>[0-9]+)/edit/$', views.edit_gift, name='edit_gift'),
    url(r'^gift/(?P<gift_id>[0-9]+)/remove/$', views.remove_gift, name='remove_gift'),

    url(r'^user/$', views.view_user, name='view_user'),

    url(r'^claims/$', views.list_claims, name='list_claims'),
    url(r'^claim/(?P<claim_id>[0-9]+)/$', views.view_claim, name='view_claim'),

    url(r'^connections/$', views.view_connections, name='view_connections'),
    url(r'^connection_request/(?P<user_id>[0-9]+)/$', views.connection_request, name='connection_request'),
    url(r'^pending_connection/(?P<connection_request_id>[0-9]+)/$', views.pending_connection, name='pending_connection'),

    url(r'^search_users/$', views.search_users, name='search_users'),

    url(r'^setup/$', views.setup, name='setup'),

    ## These were all stolen from https://github.com/django/django/blob/master/django/contrib/auth/urls.py
    url(r'^login/$', auth_views.login, {'template_name': 'main/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'main/logout.html'}, name='logout'),

]
