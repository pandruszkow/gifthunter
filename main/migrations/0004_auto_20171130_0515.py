# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-30 05:15
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0003_gift_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Claim',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateField(auto_now_add=True)),
                ('reveal_on', models.DateField()),
                ('gift', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Gift', verbose_name='Gift')),
            ],
        ),
        migrations.CreateModel(
            name='Connection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='GifthunterUser',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AddField(
            model_name='connection',
            name='giftee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='giftee', to=settings.AUTH_USER_MODEL, verbose_name='Giftee'),
        ),
        migrations.AddField(
            model_name='connection',
            name='gifter',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gifter', to=settings.AUTH_USER_MODEL, verbose_name='Gifter'),
        ),
        migrations.AddField(
            model_name='claim',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Owner'),
        ),
    ]
