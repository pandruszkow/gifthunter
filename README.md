# Gifthunter
Gift tracking and coordination for groups.

VERSION 1 IS DEPRECATED. See https://gitlab.com/TheTwitchy/gifthunter/-/tree/v2 for the current state of version 2.

## Overview
This self-hosted application allows you to create a wishlist of gifts for yourself, and to see wishlists created by other people in the system. Then, all of you can "claim" gifts that other people want, and the system will prevent two people from claiming the same gift.

## Screenshots
### Main Page

![main page](https://i.imgur.com/Dc4HIAL.png)

### Gifts List
![gifts list](https://i.imgur.com/mcuLFAK.png)

### Gift View
![gift view](https://i.imgur.com/dYl5z8z.png)

## Easy Deployment
* Use Heroku. The free tier is completely fine and works great. All commands below use the Heroku CLI.
* Clone this repo to your local machine with `git clone https://gitlab.com/TheTwitchy/gifthunter`, and enter the directory with `cd gifthunter`
* Make a new Heroku app with `heroku create PUT-AN-ID-HERE-IF-YOU-WANT-ONE`
* Attach a new Postgres database with `heroku addons:create heroku-postgresql:hobby-dev`
* Set a secret key with `heroku config:set SECRET_KEY=FACEROLL_ANY_RANDOM_STRING_HERE`
* Deploy with `git push heroku master`
* Ensure a worker dyno is running with `heroku ps:scale web=1`
* Issue `heroku open` to go to your website.
* On the first run, visit https://YOUR-SITE-URL-HERE.herokuapp.com/setup to setup the admin user.

## Hard Deployment
### Development Installation Instructions
  1. Clone this repo.
  1. Install python, python-mysqldb, and pip (if needed).
  1. Setup the MySQL (MariaDB) server. Run the following queries to setup a database and exclusive user, if needed: `CREATE DATABASE gifthunter;`, `CREATE USER 'gifthunter_user'@'localhost' IDENTIFIED BY 'SET_THIS_PASSWORD';`, `GRANT ALL PRIVILEGES ON gifthunter . * TO 'gifthunter_user'@'localhost';`, and `FLUSH PRIVILEGES;`. Note the password that needs to be set, and if the database is remote, change the `@'localhost'` in each query to `@'%'`.
  1. Run `pip install django`.
  1. Copy `gifthunter/settings.py.template`  to `gifthunter/settings.py`, set the SECRET_KEY to a different value, and set the database correctly. Make sure to make a database called 'gifthunter' (or whatever the database name is). In order to run tests the user also needs to be able to create and delete databases.
  1. Run `manage.py makemigrations` to setup the migrations files.
  1. Run `manage.py migrate` to setup the database.
  1. Run `manage.py runserver` to test the entire setup.
  1. If needed, run `manage.py createsuperuser` to setup an initial account for testing.

### Production Deployment Instructions
  1. Assuming CentOS 7.X, `yum install python python-devel python-pip virtualenv python-mysqldb nginx`. You may need to enable EPEL with `yum install epel-release`.
  1. `mkdir -p /opt/gifthunter/gh_env` which will hold the virtualenv.
  1. `virtualenv /opt/gifthunter/gh_env`
  1. Clone the repo in /opt/gifthunter/repo.
  1. `chown nginx:nginx /opt/gifthunter/repo -R`. Note that this isn't the repo itself, but the repo folder, as this is where the unix socket will live.
  1. Setup MySQL as shown in the Development Install Instructions.
  1. Enter the virtualenv with `source /opt/gifthunter/gh_env/bin/activate`
  1. `pip install django==1.11 gunicorn mysqlclients`
  1. `python manage.py collectstatic` will collect all the staticfiles at STATIC_ROOT. Specifically this will make it so that CSS files in the admin app are served correctly.
  1. Leave the virtualenv with `deactivate`.
  1. Perform the rest of the Development Install Instructions, making sure you are within the virtualenv for the migrations.
  1. `nano /etc/systemd/system/gunicorn.service`

    
    [Unit]
    Description=gunicorn daemon
    After=network.target

    [Service]
    User=nginx
    Group=nginx
    WorkingDirectory=/opt/gifthunter/repo/gifthunter
    ExecStart=/opt/gifthunter/gh_env/bin/gunicorn --workers 4 --bind unix:/opt/gifthunter/repo/gifthunter.sock gifthunter.wsgi:application

    [Install]
    WantedBy=multi-user.target
    
    
  1. `systemctl daemon-reload`
  1. `nano /etc/nginx/conf.d/gifthunter.conf`

    
    server {
        listen 80;
        server_name gifthunter.HOST.com www.gifthunter.HOST.com;

        location = /favicon.ico { access_log off; log_not_found off; }
        location /static/ {
            root /opt/gifthunter/repo/;
            etag on;
            gzip_static on;
            expires 7d;
            add_header Cache-Control private;
        }

        location / {
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass       http://unix:/opt/gifthunter/repo/gifthunter.sock;
        }
    }
    
  1. Set both services to autostart with `systemctl enable nginx` and `systemctl enable gunicorn`.
